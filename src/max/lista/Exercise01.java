package max.lista;


public class Exercise01 {
	
	public enum ArraySize {
		SIZE(20);
		
		public final int size;

		private ArraySize(int size) {
			this.size = size;
		}

		public int getSize() {
			return size;
		}		
	}
	
	private int[] myArray;

	public Exercise01() {
		super();
		myArray = new int[ArraySize.SIZE.size];
		for(int i = 0; i < myArray.length; i++) {
			myArray[i] = (i * 10);
		}
		
		System.out.println("Arranjo de 20 posi��es instanciado.\n");
		this.myArray = myArray;
	}
	
	public void showArray() {
		System.out.print("[");
		for(int i = 0; i < myArray.length; i++) {
			if(i < (myArray.length - 1)) {
			System.out.printf("%d,",myArray[i]);
			}else {
				System.out.printf("%d", myArray[i]);
			}
		}
		System.out.print("]");
	}
	
	public void inverseArray() {
		
		int[] inverseArray;
		inverseArray = new int[ArraySize.SIZE.size];
		
		System.out.println();
		
		for(int i = (myArray.length - 1); i >= 0; i--) {
			inverseArray[myArray.length - (i+1)] = myArray[i];
		}
		
		setMyArray(inverseArray);
	}
	
	public int[] getMyArray() {
		return myArray;
	}

	public void setMyArray(int[] myArray) {
		this.myArray = myArray;
	}
}
