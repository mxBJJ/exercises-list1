package max.lista;

import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		
		int[] vector = {1,3,5,7,8,9,10,2,2,5,10};
		int number = 2;
		
		callExercise1();
		callExercise2(vector,number);
		
		
		List<Integer> list1 = new ArrayList<>();
		list1.add(1);
		list1.add(2);
		list1.add(3);
		
		List<Integer> list2 = new ArrayList<>();
		list2.add(1);
		list2.add(5);
		list2.add(7);
		
		System.out.println("Union = " + Exercise02.union(list1, list2));
		System.out.println("Intersect = " + Exercise02.intersect(list1, list2));
		System.out.println("Diference = " + Exercise02.difference(list1, list2));
	}
	
	public static void callExercise1() {
		
		Exercise01 obj1 = new Exercise01();
		obj1.showArray();	
		obj1.inverseArray();
		obj1.showArray();	
		
		System.out.println();
	}
	
	public static void callExercise2(int[] vector, int number) {
		
		System.out.println("Ocorrencias do numero " + number + " = " +Exercise02.numberOfOcurrences(vector,2));
		System.out.println(Exercise02.hasRepeat(vector));
		System.out.println("Number of repeated elements : " + Exercise02.numberOfRepeated(vector));
		
		List<Integer> list = Exercise02.listRepeat(vector);
	
		System.out.print("Repeated numbers: ");
		System.out.print("[");
		for(int i = 0; i < list.size(); i++) {
			if(i == list.size() -1) {
				System.out.print(list.get(i));
			}else {
				System.out.print(list.get(i) + ",");
			}
		}
		System.out.println("]");
	}
}
