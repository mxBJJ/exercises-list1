package max.lista;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Exercise02 {
	
	public static int numberOfOcurrences(int[] vector, int number) {
		
		int count = 0;
		
		for(int item : vector) {
			if(item == number) {
				count++;
			}
		}
		
		return count;
	}
	
	public static boolean hasRepeat(int[] vector) {
		
		for(int i = 0; i < vector.length; i++) {
			for(int j = 1; j < vector.length; j++) {
				if(vector[i] == vector[j] && i != j) {
					return true;
				}
			}
		}
		
		return false;
	}
	
	public static int numberOfRepeated(int[] vector) {
		
		return listRepeat(vector).size();
	}
	
	
	public static List<Integer> listRepeat(int[] vector) {
		
			List<Integer> listRepeated = new ArrayList<>();
			
			for(int i = 0; i < vector.length; i++) {
				for(int j = 1; j < vector.length; j++) {
					if(vector[i] == vector[j] && i != j) {
						if(!listRepeated.contains(vector[i])) {
							listRepeated.add(vector[i]);
						}
					}
				}
			}
			
		return listRepeated;
	}
	
	public static List<Integer> union(List<Integer> list1, List<Integer> list2){
	
		List<Integer> union = new ArrayList<>();

		
		for(Integer item : list2) {
			union.add(item);
		}

		
		for(Integer item : list1) {
			if(!list2.contains(item)) {
				union.add(item);
			}
		}
		Collections.sort(union);
		
		return union;
	}
	
	public static List<Integer> intersect(List<Integer> list1, List<Integer> list2){
		
		List<Integer> intersect = new ArrayList<>();

		
		for(Integer item : list1) {
			if(list2.contains(item)) {
				intersect.add(item);
			}
		}
		

		Collections.sort(intersect);
		
		return intersect;
	}
	
	public static List<Integer> difference(List<Integer> list1, List<Integer> list2){
			
		List<Integer> aux = new ArrayList<>();

		
		for(Integer item : list1) {
			if(!list2.contains(item)) {
				list2.remove(item);
			
			}else {
				aux.add(item);
			}
		}
		
		for(Integer item : aux) {
			if(list2.contains(item)) {
				list2.remove(item);
			}
		}
		
		for(Integer item : aux) {
			if(list1.contains(item)) {
				list1.remove(item);
			}
		}
		
		for(Integer item : list1) {
			if(!list2.contains(item)) {
				list2.add(item);
			}
		}
		
		
		Collections.sort(list2);
		
		return list2;
	}
}